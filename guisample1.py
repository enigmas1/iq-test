from tkinter import *
import sqlite3

question = {
    "What comes next in the series? 1, 1, 2, 3, 5, 8, 13, ...": ['15', '20', '21', '30'],
    "Which number is the odd one out? 3, 5, 7, 11, 14, 17": ['3', '7', '11', '14'],
    "What is the missing number? 4, 8, 16, 32, ?, 128": ['48', '56', '64', '72'],
    "Which letter comes next in the series? A, C, E, G, ?": ['I', 'J', 'K', 'L'],
    "Mary's father has five daughters: Nana, Nene, Nini, Nono.\nWhat is the name of the fifth daughter?": ['Nunu', 'Mary', 'Nana', 'Nene'],
    "A plane crashes on the border of the U.S. and Canada.\nWhere do they bury the survivors?": ['In the U.S.', 'In Canada', 'At sea', 'Nowhere'],
    "How many months have 28 days?": ['1', '2', 'All of them', 'None of them'],
    "If you have it, you want to share it.\nIf you share it, you don't have it. What is it?": ['Money', 'A secret', 'Time', 'Knowledge'],
    "What can be broken, but is never held?": ['A promise', 'A glass', 'A heart', 'A rule'],
    "What has keys but can't open locks?": ['A map', 'A piano', 'A car', 'A house'],
    "What has a heart that doesn’t beat?": ['A dead person', 'An artichoke', 'A clock', 'A book'],
    "What is black when you buy it, red when you use it, and gray when you throw it away?": ['Coal', 'A pencil', 'A candle', 'A car'],
    "If you rearrange the letters “CIFAIPC” you would have the name of a(n)": ['City', 'Animal', 'Ocean', 'River'],
    "What can you hold in your right hand, but not in your left?": ['Your left hand', 'A pencil', 'A book', 'A cup'],
    "What gets wetter the more it dries?": ['A towel', 'Water', 'A sponge', 'Clothes'],
    "What word in the English language does the following:\nthe first two letters signify a male, the first three letters signify a female,\nthe first four letters signify a great,\nwhile the entire world signifies a great woman.\nWhat is the word?": ['Heroine', 'Female', 'Lady', 'Girl'],
    "What has a neck but no head?": ['A bottle', 'A guitar', 'A shirt', 'A person'],
    "Choose the number that is 1/4 of 1/2 of 1/5 of 200": ['2', '5', '10', '50'],
    "I speak without a mouth and hear without ears.\nI have no body, but I come alive with the wind. What am I?": ['An echo', 'A song', 'A shadow', 'A dream'],
    "What is seen in the middle of March and April \nthat can't be seen at the beginning or end of either month?": ['The letter R', 'The letter A', 'The letter M', 'The letter P']

}

ans = ['21', '14', '64', 'I', 'Mary', 'Nowhere', 'All of them', 'A secret', 'A promise', 'A piano', 'An artichoke', 'Coal', 'City', 'Your left hand', 'A towel', 'Heroine', 'A bottle', '5', 'An echo', 'The letter R']


total_questions = len(question)

# Database setup
conn = sqlite3.connect('quiz_database.db')
c = conn.cursor()

# Create a table to store user information and total scores
c.execute('''
          CREATE TABLE IF NOT EXISTS users (
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              name TEXT,
              age INTEGER,
              score INTEGER
          )
          ''')
conn.commit()

current_question = 0
def welcome():
    Label(f1, text="Welcome to IQ test",
              font="arial 30 bold",padx=20,pady=10).pack()


def login():
    login_frame.pack_forget()
    start_quiz_page()

def start_quiz_page():
    f1.pack(side=TOP, fill=X)
    start_button.pack(pady = 170)


def start_quiz():
    name = name_entry.get()
    age = age_entry.get()

    c.execute("INSERT INTO users (name, age, score) VALUES (?, ?, 0)", (name, age))
    conn.commit()

    start_button.pack_forget()
    next_button.pack()
    next_question()


def next_question():
    global current_question
    if current_question < total_questions:
        check_ans()
        user_ans.set('None')
        c_question = list(question.keys())[current_question]
        clear_frame()
        Label(f1, text=f"Q) {c_question}", padx=12,pady=17,
              font="arial 23 bold",fg="#003300").pack(anchor=NW)
        for option in question[c_question]:
            Radiobutton(f1, text=option, variable=user_ans,
                        value=option, padx=28,pady=17,font="arial 23 normal",fg="#999966").pack(anchor=NW)
        current_question += 1
    else:
        next_button.forget()
        check_ans()
        clear_frame()
        output = f"{name_entry.get()} your Score is {user_score.get()} out of {total_questions}"
        Label(f1, text=output, font="arial 30 bold",pady=50).pack()
        Label(f1, text="Thanks for Participating",
              font="arial 30 bold").pack()


def check_ans():
    temp_ans = user_ans.get()
    if temp_ans != 'None' and temp_ans == ans[current_question - 1]:
        user_score.set(user_score.get() + 1)
        user_id = c.lastrowid
        c.execute("UPDATE users SET score = ? WHERE id = ?", (user_score.get(), user_id))
        conn.commit()


def clear_frame():
    for widget in f1.winfo_children():
        widget.destroy()


if __name__ == "__main__":
    root = Tk()
    root.title("IQ TEST")
    root.geometry("10x10")
    root.minsize(1300,780)
    logo_image = PhotoImage(file="pic.png") 
    root.iconphoto(True,logo_image)

    user_ans = StringVar()
    user_ans.set('None')
    user_score = IntVar()
    user_score.set(0)
    f1 = Frame(root)
    f1.pack(side=TOP, fill=X)
    welcome()

    login_frame = Frame(root)
    login_frame.pack(side=TOP, fill=X, pady = 150)
    Label(login_frame, text="Enter your name:",font="consolas 20 bold",fg="#333300").pack()
    name_entry = Entry(login_frame,insertwidth=10)
    name_entry.pack()
    Label(login_frame, text="Enter your age:",font="consolas 20 bold",fg="#333300").pack()
    age_entry = Entry(login_frame,insertwidth=10)
    age_entry.pack()
    login_button = Button(login_frame, text="Login", command=login, font="consolas 17 bold",fg="#333300")
    login_button.pack()

    
    

    start_button = Button(root, text="Start TEST",
                          command=start_quiz,
                          font="calibre 17 bold")
    next_button = Button(root, text="Next Question",
                         command=next_question,
                         font="calibre 17 bold",fg="#339933")

    root.mainloop()
